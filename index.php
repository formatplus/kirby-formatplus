<?php

Kirby::plugin('formatplus/formatplus', [
  // creates user pages virtually
 'routes' => [
   [
     'pattern' => 'autoren/(:any)',
     'action' => function ($value) {
       $data = [
         'currentUser' => urldecode($value),
       ];
       return page('autoren')->render($data);
     }
   ]
  ],
  
]);
